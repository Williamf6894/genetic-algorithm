package com.williamf6894.example;

import java.util.ArrayList;
import java.util.Arrays;

class Population {

	private Ordering[] population; // Population of one Generations
	private Ordering[] matingPool; // Orderings from the Generations to move to the next.
	private int totalPopulation; 
    private  int students = 0;
	private  int[][] arrayOfCourses;
    private  int courseModules = 0;
	String best;
	boolean finished = false;

    Population() {

        Config config = Config.getInstance();

        this.totalPopulation = config.getNumberOfAnswersToGenerate();
        this.students = config.getTotalNumberOfStudents();
        this.courseModules = config.getNumberOfModulesPerCourse();
        int modulesTotal = config.getTotalNumberOfModules();

        this.arrayOfCourses = new int[this.students][this.courseModules];


		population = new Ordering[totalPopulation];
        ArrayList<Integer> modsArray = new ArrayList<>();
        boolean numberIsUnique = false;
		int modNumber = 0;

        // Create a multidimensional array, for all the students and their modules.
        // It doesn't matter if there is no course for a module, however, it must still be in the schedule
        // Courses can share modules, but cannot be the exact same

        for (int i = 0; i < students; i++) {
            for (int j = 0; j < this.courseModules; j++) {

				// Making sure that none are repeating
                while (!numberIsUnique) {

                    modNumber = (int) (Math.random() * modulesTotal) + 1;
                    if (!modsArray.contains(modNumber)) {
						modsArray.add(modNumber);
	                    numberIsUnique = true;
                    }
                }
                arrayOfCourses[i][j] = modNumber;
				numberIsUnique = false;
            }

			modsArray.clear();
            Arrays.sort(arrayOfCourses[i]);

        }
        // Created Array for each student and their modules, with no duplicated mods.

        // Creating the first Generation.
		for (int i = 0; i < totalPopulation; i++) {
			this.population[i] = new Ordering(arrayOfCourses);
        }

        // Get the Fitness of the Generation
		calculateTotalFitness();

	}


	String printStudents() {

		StringBuilder res = new StringBuilder("\n");
		for (int i =0; i < students; i++) {

			res.append("Student ").append(i + 1).append(": ");
            for (int j = 0; j < this.courseModules; j++) {
				res.append("M").append(arrayOfCourses[i][j]).append(" ");
            }
			res.append("\n");
		}

		res.append("\n");
		return res.toString();
	}

    // Take the best results and put them in the next generation
    void naturalSelection() {

		int count = 0;
		this.matingPool = null;
		Ordering[] popClone = population.clone();
        Ordering[] selectedPop = population.clone();
        
        // Sort through, putting the fittest first.
        // Then increment count, and add the next fittest.
        // Do until done. Now sorted by fitness. 
		for(int i = 0; i < totalPopulation; i++) {
			for(int j = 0;  j < totalPopulation; j++) {
				int fitn = (int) this.population[j].fitness;
				if( fitn == count){
					selectedPop[i] = popClone[j];
					popClone[j] = null;
					i++;
				}
			}
			i--;
			count++;
		}

        // Since sorted. Take the best third and the worst third.
		int firstSize = totalPopulation / 3;
		int thirdSize = totalPopulation - firstSize;

        // Replace the worst third with the best third. 1,2,3 -> 1,2,1
		for(int third = 0; third < firstSize; third++){
			selectedPop[(third+thirdSize)] = selectedPop[third];
		}

		this.matingPool = selectedPop.clone();

	}

	void generateNextGeneration() {
        // From the MatingPool, we create the next Generation with modifications 


		int randomNumber; // Hardcoded RNG values for each process
		this.population = new Ordering[totalPopulation];
		for(int i = 0; i < this.totalPopulation; i++){
            randomNumber = (int) (Math.random() * 20);
            
			// Reproduction
            if (randomNumber <= 16) {
                this.population[i] = this.matingPool[i];
            }
            
			// Crossover
            // if randomNumber is 18, 19, 20 
            else if (randomNumber > 17 && (i + 1) < totalPopulation) {

                // Each Crossover generates one, so run it twice on the pair
                this.population[i] = this.matingPool[i].crossover(this.matingPool[i + 1]);
                this.population[i + 1] = this.matingPool[i + 1].crossover(this.matingPool[i]);

                i++;
            }
            
            // Mutation
            // randomNumber is 17 or 5%;
			else {  
				this.population[i] = this.matingPool[i].mutation();
			}


		}
	}


    // Check the Generation for a perfect result
    // Therefore complete.
    void evaluate() {

        finished = false;

        for (int i = 0; i < totalPopulation; i++) {
            if (this.population[i].fitness == 0.0) {
                finished = true;
                this.best = this.population[i].toString();
                break;
            }
        }
    }
    

    void calculateTotalFitness() {

        // For all Ordering, it gets the Fitness
        for (int i = 0; i < totalPopulation; i++)
            this.population[i].calcFitness(arrayOfCourses);

    }
    

    public String toString() {

        StringBuilder result = new StringBuilder();

        for (int ind = 0; ind < totalPopulation; ind++) {

            result.append("Ord ").append(ind + 1).append(":\n");
            result.append(this.population[ind].toString());

        }
        return result.toString();
    }
    
}