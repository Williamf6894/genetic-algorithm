package com.williamf6894.example;

class Config {

    // Default values
    private int numberOfGenerations;
    private int numberOfAnswersToGenerate;
    private int totalNumberOfStudents;
    private int totalNumberOfModules;
    private int numberOfModulesPerCourse;

    private Config() {}

    private static final Config CONFIG = new Config();

    static Config getInstance(){
        return CONFIG;
    }

    /**
     * @return the numberOfModulesPerCourse
     */
    int getNumberOfModulesPerCourse() {
        return this.numberOfModulesPerCourse;
    }

    /**
     * @param numberOfModulesPerCourse the numberOfModulesPerCourse to set
     */
    void setNumberOfModulesPerCourse(int numberOfModulesPerCourse) {
        this.numberOfModulesPerCourse = numberOfModulesPerCourse;
    }

    /**
     * @return the totalNumberOfModules
     */
    int getTotalNumberOfModules() {
        return this.totalNumberOfModules;
    }

    /**
     * @param totalNumberOfModules the totalNumberOfModules to set
     */
    void setTotalNumberOfModules(int totalNumberOfModules) {
        this.totalNumberOfModules = totalNumberOfModules;
    }

    /**
     * @return the totalNumberOfStudents
     */
    int getTotalNumberOfStudents() {
        return this.totalNumberOfStudents;
    }

    /**
     * @param totalNumberOfStudents the totalNumberOfStudents to set
     */
    void setTotalNumberOfStudents(int totalNumberOfStudents) {
        this.totalNumberOfStudents = totalNumberOfStudents;
    }

    /**
     * @return the numberOfAnswersToGenerate
     */
    int getNumberOfAnswersToGenerate() {
        return this.numberOfAnswersToGenerate;
    }

    /**
     * @param numberOfAnswersToGenerate the numberOfAnswersToGenerate to set
     */
    void setNumberOfAnswersToGenerate(int numberOfAnswersToGenerate) {
        this.numberOfAnswersToGenerate = numberOfAnswersToGenerate;
    }

    /**
     * @return the numberOfGenerations
     */
    int getNumberOfGenerations() {
        return this.numberOfGenerations;
    }

    /**
     * @param numberOfGenerations the numberOfGenerations to set
     */
    void setNumberOfGenerations(int numberOfGenerations) {
        this.numberOfGenerations = numberOfGenerations;
    }

}