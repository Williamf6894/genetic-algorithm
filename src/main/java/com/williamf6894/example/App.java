package com.williamf6894.example;

import java.io.File;
import java.io.FileWriter;


/**
 * Make a genetic algorithm that will create an exam schedule for students. The
 * students can take any module. There is a set number of modules in total that
 * the students can pick from There is a max number of modules that students can
 * take, and they can take modules that others are taking. The exam halls will
 * have two exams for each session. Since there are two per session, there needs
 * to be an even number of modules in total. There must not be any conflicts in
 * the end result.
 * 
 * Make sure that there is a rating system for the genetic algorithm to improve.
 * There must be new generations too.
 *
 * There need to be a Natural Selection implementation
 * There must be a crossover function and a mutation function.
 * 
 */
public class App {
    public static void main(String[] args) {

        MenuScreen.startFakeMenuScreen();
        Config config = MenuScreen.getConfiguration();
        System.out.println(config.getNumberOfAnswersToGenerate());

        // I think it would be better to print to the console and just use standard output.
        StringBuilder allResults;

        Population population = new Population();
        allResults = new StringBuilder(population.printStudents());

        for (int gen = 0; gen < config.getNumberOfGenerations(); gen++) {
            allResults.append("Generation: ").append(gen).append("\n");

            population.calculateTotalFitness();
            population.evaluate();

            allResults.append(population.toString());

            population.naturalSelection();
            population.generateNextGeneration();

            if (population.finished) {
                allResults.append("\nSuccess found!\nGeneration ").append(gen).append("\n");
                allResults.append(population.best);

                System.out.println("\nSuccess found!\nGeneration " + gen);
                System.out.println(population.best);

                break;
            }

        }

        writeResultsToFile(allResults);

    }

    private static void writeResultsToFile(StringBuilder results){

        try {

            File file = new File("results_logfile.txt");
            FileWriter fw = new FileWriter(file);

            fw.write(results.toString());
            fw.flush();
            fw.close();

        } catch (Exception e) {
            System.err.println("There was a problem with importing the results file.");
        }

    }

}
