package com.williamf6894.example;

import java.util.ArrayList;
import java.util.Collections;

class Ordering {

	private Config config = Config.getInstance();
	double fitness = 0;                      // Fitness: Low is best
    private int[][] arrayOfCourses;                  // Each course has X mods
	private int modulesTotal = config.getTotalNumberOfModules();    // Count of all Mods
	private int[] exams = new int[modulesTotal];	 // Each Module has an exam time.
    private int examsPerDay = 2;
	private int days = (modulesTotal / examsPerDay); // 2 exams per day


    Ordering(int[][] arrayOfCourses) {

        int examsPerDay = 2;
        this.arrayOfCourses = arrayOfCourses;
        this.days = (modulesTotal / examsPerDay);	// 2 exams per day

		generateOrdering(modulesTotal);
    }
    
    /**
     * This generates and shuffles an ArrayList of all the modules.
     * @param modulesTotal Is the total number of Modules also stored in Config
     */
    private void generateOrdering(int modulesTotal) {

        // Using ArrayList to prevent duplications of mods
        // Mods are represented by an int
        ArrayList<Integer> modulesArray = new ArrayList<>();
        for (int i = 0; i < modulesTotal; i++) {
            modulesArray.add(i + 1);
        }

        // Randomises the modulesArray
        Collections.shuffle(modulesArray); 

        int index = modulesArray.size() - 1;

        // Iterate down and add to exam
        // 1D Int Array, adding two per loop.
        for (int day = 0; day < this.days; day++) {

            // This will iterate though the ArrayList and remove them
            // from modulesArray and add to the exam timetable 2D array.
            this.exams[day] = (int) modulesArray.remove(index);
            index--;
            this.exams[day + days] = (int) modulesArray.remove(index);
            index--;

        }
    }

    // I should write a test for these
    void calcFitness(int[][] arrayOfCourses) {
        
        int cost = 0;

		for (int day = 0; day < this.days; day++) {

			// Check the first exam each day against the next.
			int firstExam = exams[day];
            int secondExam = exams[day+days];
            

            // Iterate through the Array of Courses 
            for (int i = 0; i < arrayOfCourses.length; i++) {
                for (int j = 0; j < arrayOfCourses[i].length; j++) {

                    // For each of the firstExams, check all other second exams
                    // for incompatibility with shared courses.

                    // If the firstExam is in this course, check if the secondExam on that day, is also in the same course.
                    // If it is, increase cost by one.
                    if (firstExam == arrayOfCourses[i][j]) {

                        for (int k = 0; k < arrayOfCourses[i].length; k++) {

                            if (secondExam == arrayOfCourses[i][k]) {

								cost++;
							}
						}
					}
				}
			}
		}

		// Finally assign the cost as the fitness. 
		this.fitness = cost;

	}

	// Crossover has 15%, Rep is 80% and Mutation is 5% //
	/**
	 * From this and the parameter, a child ordering is created using crossover
	 * Its recommended to call this method from both orderings, each changing place as the parameter
	 * e.g Ordering1.crossover(Ordering2) and Ordering2.crossover(Ordering1)
	 * @param ordering2 Is the second Ordering Parent
	 * @return Ordering Child Ordering returned from the parent that called this method and the parameter
	 */
	Ordering crossover(Ordering ordering2){
		
		// The 'this' ordering is Ordering1, and the parameter is ordering2.
		// Using these two we select a random cut point near the middle.
		// From the two Parents we form a new Child Ordering

		int totalDays = this.days * 2;

		// Getting the cut point on where to divide both Orderings
		int cutPoint = (int)((Math.random() * (totalDays-5)) + 2);

		// Temp Ordering for replacing.
		Ordering childOrd = new Ordering(this.arrayOfCourses);

		for(int i = 0; i < totalDays; i++) {
			if (i < cutPoint) {
				childOrd.exams[i] = this.exams[i];
			} else {
				childOrd.exams[i] = ordering2.exams[i];
			}
		}

		// Since the crossover is prone to having missing mods
		// As well as duplicates, we have to make an ArrayList
		// to gather the missing mods and then replace the dupes.
		
		// Fill with unique
		boolean foundValue = false;
		ArrayList<Integer> remainingMods = new ArrayList<>();

		// Gather all the missing mods
		for (int i = 1; i < modulesTotal+1; i++) {
			foundValue = false; // Reset foundValue
			for(int j = 0; j < childOrd.exams.length; j++)
				if (i == childOrd.exams[j])
					foundValue = true;

			if (!foundValue) {
				remainingMods.add(i);
			}
		}

		// Now find the dupes and replaces them with something from the
		// missing ArrayList
		for(int j = 0; j < totalDays; j++){
			for (int k = 0; k < totalDays; k++) {

				if (j != k && childOrd.exams[j] == childOrd.exams[k] && !(remainingMods.isEmpty())) {
					
					childOrd.exams[j] = remainingMods.remove(0);
					break; // Go to next in j loop
				}
			}
		}

		return childOrd;

	}

	// Swap the location of two number in the exams array.
	Ordering mutation(){

		Ordering child = this;

		// Getting a random location of both.
		int randomPoint1 = (int)(Math.random() * this.exams.length);
		int randomPoint2 = (int)(Math.random() * this.exams.length);

		// Temp store both the values at each location
		int val1 = this.exams[randomPoint1];
		int val2 = this.exams[randomPoint2];

		// Swap the values for each position
		child.exams[randomPoint1] = val2;
		child.exams[randomPoint2] = val1;

		// Return the new ordering
		return child;

	}

	// Small toString function to display the Ordering in a terminal
	public String toString(){
		// Iterate down and add to exam
		// Display
		// Using StringBuilder for efficiency
		StringBuilder resultString = new StringBuilder();

		for (int day = 0; day < this.days; day++) {
			// M for Module.
			resultString.append("M").append(exams[day]).append("\t");
		}

		resultString.append("\n");
		
		// First half done, now second half.
		for (int day = 0; day < this.days; day++) {
			resultString.append("M").append(exams[day + days]).append("\t");
		}

 		this.calcFitness(arrayOfCourses);
		resultString.append(": cost: ").append(this.fitness).append("\n\n");
		return resultString.toString();
	}

}