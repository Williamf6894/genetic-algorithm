package com.williamf6894.example;

import java.util.Scanner;

public class MenuScreen {

    // Default values set here
    private static int numberOfGenerations = 100;
    private static int numberOfAnswersToGenerate = 25;
    private static int totalNumberOfStudents = 30;
    private static int totalNumberOfModules = 50;
    private static int numberOfModulesPerCourse = 5;

    public MenuScreen() {

    }

    static void startFakeMenuScreen() {
        System.out.println("If you need help, please type 'help'");

        print("This is the fake menu screen, default values used.");

        System.out.println("\n\n");

    }

    private static void startMenuScreen() {
        Scanner in = new Scanner(System.in);
        System.out.println("If you need help, please type 'help'");

        numberOfGenerations = getIntFromInput(in, "Please enter the total amount of generations you want: ");

        numberOfAnswersToGenerate = getIntFromInput(in, "Please enter the number of answers to generate for each generation: ");

        totalNumberOfStudents = getIntFromInput(in, "Please enter the amount of students: ");

        totalNumberOfModules = getIntFromInput(in, "Please enter the number of modules in total: ");
        totalNumberOfModules = checkAndReturnEvenNumber(totalNumberOfModules);

        numberOfModulesPerCourse = getIntFromInput(in, "Please enter how many modules per course: ");

        System.out.println("\n\n");
        in.close();
        if (numberOfModulesPerCourse > totalNumberOfModules)
            startMenuScreen();
    }


    static Config getConfiguration() {
        Config configuration = Config.getInstance();

        configuration.setNumberOfAnswersToGenerate(numberOfAnswersToGenerate);
        configuration.setNumberOfGenerations(numberOfGenerations);
        configuration.setTotalNumberOfStudents(totalNumberOfStudents);
        configuration.setTotalNumberOfModules(totalNumberOfModules);
        configuration.setNumberOfModulesPerCourse(numberOfModulesPerCourse);

        return configuration;
    }


    private static int getIntFromInput(Scanner in, String output) {
        String inputString;
        int inputNumberToReturn;

        boolean recheck = true; // Looping flag for retrying in-case of bad input.

        while (recheck) {

            print(output);
            inputString = in.nextLine();

            // Check its the user is looking for the help pages
            if (checkIfInputMatchesKeyword("help", inputString)) {
                printHelpPage();
            }
            // Check if its a digit using regex
            else if (inputString.matches(".*\\d+.*")) {

                inputNumberToReturn = Integer.parseInt(inputString);
                if (inputNumberToReturn > 0) {
                    return inputNumberToReturn;
                }

            } else if (checkIfInputMatchesKeyword("exit", inputString)) {
                recheck = false;    // Method of cancelling.

            } else {
                System.out.println("Please enter in a whole number");
            }
        }
        return 0; // Will reach on 'exit'
    }
    

    static boolean checkIfInputMatchesKeyword(String keyword, String input) {
        keyword = keyword.toLowerCase();
        return input.matches(keyword);
    }


    static int checkAndReturnEvenNumber(int numberToCheck) {
        // Making sure the number is even
        // If it isn't then we round up, using a false student.
        
        if (numberToCheck % 2 == 0)
            return numberToCheck;
        else
            return ++numberToCheck;
    }


    private static void printHelpPage() {
        print("Something for the help page to be made");
        print("placeholder");
    }
    

    private static void print(String printText) {
        // All this does is shorten System.out.println to print
        System.out.println(printText);
    }

}