package com.williamf6894.example;

import org.junit.Test;

import static com.williamf6894.example.MenuScreen.checkAndReturnEvenNumber;
import static com.williamf6894.example.MenuScreen.checkIfInputMatchesKeyword;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class MenuScreenTest {

    /**
     * Rigorous Test :-)
     */
    @Test
    public void getIntFromInput_Should_Retry_Until_Correct() {
    // Probably going to remove this one.
        assertTrue(true);

    }

    @Test
    public void checkAndReturnEvenNumber_Should_Always_Return_Even_Number() {
        int result = checkAndReturnEvenNumber(5);
        assertEquals(6, result);

        result = checkAndReturnEvenNumber(8);
        assertEquals(8, result);
    }

    @Test
    public void checkIfInputMatchesKeyword_Should_Match_Exact_Word() {
        boolean testResult;
        
        testResult = checkIfInputMatchesKeyword("exit", "exit");
        assertTrue(testResult);

        testResult = checkIfInputMatchesKeyword("Exit", "exit");
        assertTrue(testResult);

        testResult = checkIfInputMatchesKeyword("xeti", "exit");
        assertTrue(!testResult);

    }

}
